/**
 * @file
 * Defines an integer sequence type and a factory type for creating arithmetic sequences of integers.
 * This code is largely based on http://stackoverflow.com/a/32223343
 * @author: Stijn Manhaeve, 2016
 */

#ifndef INCLUDE_ADAPT2RFP_INTSEQUENCE_H_
#define INCLUDE_ADAPT2RFP_INTSEQUENCE_H_

//------
// Helps with splitting the parameter pack
// based on http://stackoverflow.com/a/32223343
//------
namespace n_sequence {
    /**
     * @brief Basic template type for integer sequences.
     * @tparam N An integer type. This type is used to fill the sequence.
     * @tparam vals A sequence of values of type N. This is the actual integer sequence.
     */
    template<typename N, N... vals>
    struct Sequence {
        typedef Sequence<N, vals...> type;
        typedef N valueType;

        //just returns the number of arguments
        /**
         * @brief Returns the length of the integer sequence.
         */
        static constexpr std::size_t
        size() noexcept
        {
            return sizeof...(vals);
        }
    };

    namespace n_impl {

        //merge two sequences
        /**
         * @brief Struct for merging two integer arithmetic sequences
         * @tparam N An integer type. This is the type of the elements in the sequence.
         * @tparam start The starting value of the arithmetic sequence
         * @tparam diff The common difference of the arithmetic sequence
         * @tparam S1 The first arithmetic sequence of integers
         * @tparam S2 The second arithmetic sequence of integers
         */
        template<typename N, N start, N diff, typename S1, typename S2>
        struct MergeArithmeticSequence;

        template<typename N, N start, N diff, N... S1, N... S2>
        struct MergeArithmeticSequence<N, start, diff, Sequence<N, S1...>, Sequence<N, S2...>>:
            Sequence<N, S1..., (start + sizeof...(S1)*diff+(S2-start))...>
        {
        };

        //actually make the sequences
        /**
         * @brief Creates an arithmetic sequence of a certain length.
         * @tparam N An integer type. This is the type of the elements in the sequence.
         * @tparam start The initial value of the arithmetic sequence
         * @tparam diff The common difference of the arithmetic sequence
         * @tparam amount The length of the arithmetic sequence
         * For example:
         * @code
         *  $ MakeSequence<unsigned int, 1, 3, 6>::type()
         *  > Sequence<unsigned int, 1, 4, 7, 10, 13, 16>
         *  $ MakeSequence<int, -1, 2, 5>::type()
         *  > Sequence<int, -1, 1, 3, 5, 7>
         *  $ MakeSequence<std::uint8_t, 0, 113, 5>::type()
         *  > Sequence<std::uint8_t, 0, 113, 226, 83, 196>
         * @endcode
         * @complexity
         * The number of expanded templates is O(log(n)), where n is the length of the sequence.
         */
        template<typename N, N start, N diff, std::size_t amount>
        struct MakeSequence:
            MergeArithmeticSequence<N, start, diff,
                typename MakeSequence<N, start, diff, amount / 2>::type,
                typename MakeSequence<N, start, diff, amount - amount / 2>::type>
        {
        };

        // base case for a sequence of length 0
        template<typename N, N start, N diff>
        struct MakeSequence<N, start, diff, 0>:
            Sequence<N>
        {
        };

        // base case for a sequence of length 1
        template<typename N, N start, N diff>
        struct MakeSequence<N, start, diff, 1>:
            Sequence<N, start>
        {
        };

    } /* namespace n_impl */

    //just a typedef that switches around the template arguments && converts to the base type
    /**
     * @brief Creates an arithmetic sequence of a certain length.
     * @tparam N An integer type. This is the type of the elements in the sequence.
     * @tparam amount The length of the arithmetic sequence
     * @tparam start The initial value of the arithmetic sequence. Default value is N(0)
     * @tparam diff The common difference of the arithmetic sequence. Default value is N(1)
     * For example:
     * @code
     *  $ ArithmeticSequence<unsigned int, 6, 1, 3>
     *  > Sequence<unsigned int, 1, 4, 7, 10, 13, 16>
     *  $ ArithmeticSequence<int, 5, -1, 2>
     *  > Sequence<int, -1, 1, 3, 5, 7>
     *  $ ArithmeticSequence<std::uint8_t, 5, 0, 113>
     *  > Sequence<std::uint8_t, 0, 113, 226, 83, 196>
     * @endcode
     */
    template<typename N, std::size_t amount, N start = N(0), N diff = N(1)>
    using ArithmeticSequence = typename n_impl::MakeSequence<N, start, diff, amount>::type;

    /**
     * @brief A sequence of even numbers, starting at 0.
     * @tparam N An integer type. This is the type of the elements in the sequence.
     * @tparam amount The length of the arithmetic sequence
     * For example:
     * @code
     *  $ EvenSequence<unsigned int, 6>
     *  > Sequence<unsigned int, 0, 2, 4, 6, 8, 10>
     * @endcode
     */
    template<typename N, std::size_t amount>
    using EvenSequence = ArithmeticSequence<N, amount, N(0), N(2)>;

    /**
     * @brief A sequence of odd numbers, starting at 1.
     * @tparam N An integer type. This is the type of the elements in the sequence.
     * @tparam amount The length of the arithmetic sequence
     * For example:
     * @code
     *  $ OddSequence<unsigned int, 5>
     *  > Sequence<unsigned int, 1, 3, 5, 7, 9>
     * @endcode
     */
    template<typename N, std::size_t amount>
    using OddSequence = ArithmeticSequence<N, amount, N(1), N(2)>;

    template<std::size_t N, typename... T>
    struct getTypeImpl
    {
            //use static_assert to make compiler errors more friendly.
            static_assert(N < sizeof...(T), "Tuple access index out of bounds.");
            typedef typename std::tuple_element<N, std::tuple<T...>>::type type;
    };

    // using a struct first allows us to use the static_assert bounds check
    /**
     * @brief The N-th type in a parameter pack
     * @tparam N The index of the type you want to get. The parameter pack is zero-indexed.
     * @tparam T The type parameter pack
     * @precondition The index may not be out of bounds. That is, `N < sizeof...(T)`
     */
    template<std::size_t N, typename... T>
    using getType = typename getTypeImpl<N, T...>::type;

    /**
     * @brief builds a tuple type based on an initial tuple type and Sequence type
     * @tparam T The tuple type that delivers the types to choose from
     * @tparam N A Sequence type that dictates in which order the types from T are selected for the resulting tuple type
     *          Values in this sequence may be repeated or missing.
     * @precondition All indexes are within bounds. That is, for each value `n` in the sequence of `N`: `n < std::tuple_size<T>`
     */
    template<typename T, typename N>
    struct filterType
    {
    };

    template<typename... T, std::size_t... N>
    struct filterType<std::tuple<T...>, Sequence<std::size_t, N...>>
    {
            typedef std::tuple<n_sequence::getType<N, T...>...> type;
    };
    //We could expand the number of sequences, but this more than covers what I need: a sequence of even and uneven numbers
} /* namespace n_sequence */

//further plans: use these sequences to break the parameter pack into two

#endif /* INCLUDE_ADAPT2RFP_INTSEQUENCE_H_ */
