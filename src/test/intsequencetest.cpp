/**
* @file
* Implementation of unit tests for integer sequences.
*
* @author: Stijn Manhaeve, 2016
*/

#include <type_traits>
#include <cstdint>
#include "gtest.h"
#include "templates/intsequence.h"

using namespace n_sequence;

namespace UnitTests {
    TEST( SequenceTest, sequenceSize )
    {
        //need to compute it here because gtest macro screws up the template arguments
        std::size_t size = 0;
#define SizeTEST(t, a, b, s) do{\
        size = ArithmeticSequence<t, s, a, b>::size();\
        EXPECT_EQ( size, s );} while(0)
#define BigSizeTEST(t, s) do{\
        SizeTEST(t, 0, 0, s);\
        SizeTEST(t, 0, 1, s);\
        SizeTEST(t, 0, 2, s);} while(0)

        BigSizeTEST(int, 0);
        BigSizeTEST(int, 1);
        BigSizeTEST(int, 2);
        BigSizeTEST(char, 0);
        BigSizeTEST(char, 1);
        BigSizeTEST(char, 2);
#undef SizeTEST
#undef BigSizeTEST
    }

    TEST( SequenceTest, sequenceValues )
    {
        bool value = std::is_same<ArithmeticSequence<std::size_t, 6, 1, 3>, Sequence<std::size_t, 1, 4, 7, 10, 13, 16>>::value;
        EXPECT_TRUE(value);
        value = std::is_same<ArithmeticSequence<int, 5, -1, 2>, Sequence<int, -1, 1, 3, 5, 7>>::value;
        EXPECT_TRUE(value);
        value = std::is_same<ArithmeticSequence<std::uint8_t, 5, 0, 113>, Sequence<std::uint8_t, 0, 113, 226, 83, 196>>::value;
        EXPECT_TRUE(value);
        //show that order matters
        value = std::is_same<ArithmeticSequence<int, 6, 1, 3>, ArithmeticSequence<int, 6, 16, -3>>::value;
        EXPECT_FALSE(value);
        //show that type matters
        value = std::is_same<ArithmeticSequence<int, 6, 1, 3>, ArithmeticSequence<unsigned int, 6, 1, 3>>::value;
        EXPECT_FALSE(value);
        //show that length matters
        value = std::is_same<ArithmeticSequence<int, 6, 1, 3>, ArithmeticSequence<int, 7, 1, 3>>::value;
        EXPECT_FALSE(value);
        //show that values matter
        value = std::is_same<ArithmeticSequence<int, 6, 1, 3>, ArithmeticSequence<int, 7, 12, -1>>::value;
        EXPECT_FALSE(value);
    }
    TEST( SequenceTest, specialSequences )
    {
        bool value = false;
        value = std::is_same<EvenSequence<unsigned int, 6>, Sequence<unsigned int, 0, 2, 4, 6, 8, 10>>::value;
        EXPECT_TRUE(value);
        value = std::is_same<OddSequence<unsigned int, 5>, Sequence<unsigned int, 1, 3, 5, 7, 9>>::value;
        EXPECT_TRUE(value);
    }

    /**
     * @brief Simple struct to generate different types at will
     */
    template<std::size_t N>
    struct tstr{};

    TEST( SequenceTest, parampackHelpers )
    {
        bool value = false;
        value = std::is_same<getType<0, char, int, double>, char>::value;
        EXPECT_TRUE(value);
        value = std::is_same<getType<1, char, int, double>, int>::value;
        EXPECT_TRUE(value);
        value = std::is_same<getType<2, char, int, double>, double>::value;
        EXPECT_TRUE(value);
        //should not compile!
//            value = std::is_same<getType<3, char, int, double>, void*>::value;
//            EXPECT_TRUE(value);
        //regular filter
        typedef typename filterType<
                    std::tuple<tstr<0>, tstr<1>, tstr<2>, tstr<3>, tstr<4>, tstr<5>, tstr<6>>,
                    Sequence<std::size_t, 1, 3>>::type
                filter1;
        value = std::is_same<filter1, std::tuple<tstr<1>, tstr<3>>>::value;
        EXPECT_TRUE(value);
        //empty filter
        typedef typename filterType<
                    std::tuple<tstr<0>, tstr<1>, tstr<2>, tstr<3>, tstr<4>, tstr<5>, tstr<6>>,
                    Sequence<std::size_t>>::type
                filter2;
        value = std::is_same<filter2, std::tuple<>>::value;
        EXPECT_TRUE(value);
        //filter with repeating values
        typedef typename filterType<
                    std::tuple<tstr<0>, tstr<1>, tstr<2>, tstr<3>, tstr<4>, tstr<5>, tstr<6>>,
                    Sequence<std::size_t, 0, 2, 0, 3>>::type
                filter3;
        value = std::is_same<filter3, std::tuple<tstr<0>, tstr<2>, tstr<0>, tstr<3>>>::value;
        EXPECT_TRUE(value);
        //order is important
        typedef typename filterType<
                    std::tuple<tstr<0>, tstr<1>, tstr<2>, tstr<3>, tstr<4>, tstr<5>, tstr<6>>,
                    Sequence<std::size_t, 0, 1, 3>>::type
                filter4a;
        typedef typename filterType<
                    std::tuple<tstr<0>, tstr<1>, tstr<2>, tstr<3>, tstr<4>, tstr<5>, tstr<6>>,
                    Sequence<std::size_t, 3, 0, 1>>::type
                filter4b;
        value = std::is_same<filter4a, std::tuple<tstr<0>, tstr<1>, tstr<3>>>::value;
        EXPECT_TRUE(value);
        value = std::is_same<filter4b, std::tuple<tstr<3>, tstr<0>, tstr<1>>>::value;
        EXPECT_TRUE(value);
        value = std::is_same<filter4a, filter4b>::value;
        EXPECT_FALSE(value);
        //should not compile!
//            typedef typename filterType<
//                        std::tuple<tstr<0>, tstr<1>, tstr<2>, tstr<3>, tstr<4>, tstr<5>, tstr<6>>,
//                        Sequence<std::size_t, 7>>::type
//                    filter5;
        //filter can be longer than the original
        typedef typename filterType<
                    std::tuple<tstr<0>, tstr<1>, tstr<2>, tstr<3>, tstr<4>, tstr<5>, tstr<6>>,
                    Sequence<std::size_t, 0, 2, 0, 3, 0, 4, 0, 5, 0, 6>>::type
                filter6;
        value = std::is_same<filter6, std::tuple<tstr<0>, tstr<2>, tstr<0>, tstr<3>, tstr<0>, tstr<4>, tstr<0>, tstr<5>, tstr<0>, tstr<6>>>::value;
        EXPECT_TRUE(value);
    }
}   /* namespace UnitTests */

